package com.investimentos.cliente;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
	
	public Optional<Cliente> findByCpf(String cpf);
}
